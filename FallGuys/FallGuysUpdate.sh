#!/bin/bash
#author: Jhonatan A.
#email: joas.dev@gmail.com
#version: 1.0.0

# Specifies the main directory of the game
# or you can use $(pwd) if the script is in the root of the game
WORK_DIR=$(pwd)

EAC_DIR="EasyAntiCheat"
EAC_SO_FILE="easyanticheat_x64.so"

GAME_DATA_DIR="FallGuys_client_game_Data/Plugins/x86_64"
GAME_INIT_FILE="FallGuys_client.ini"
GAME_EAC_LAUNCHER="FallGuysEACLauncher.exe"

CLIENT_GAME="FallGuys_client_game.exe"

EAC_PATH="${WORK_DIR}/${EAC_DIR}/${EAC_SO_FILE}"
GAME_DATA_PATH="${WORK_DIR}/${GAME_DATA_DIR}/${EAC_SO_FILE}"

echo "Game PATH: ${GAME_DATA_PATH}"
echo "Preparing directory changes: ${WORK_DIR}"

# Remove Anticheat SO
if [ -f "$GAME_DATA_PATH" ] || [ -L "$GAME_DATA_PATH" ];
then
    rm "${GAME_DATA_PATH}"
    echo "Remove ${EAC_SO_FILE}"
fi

# Replace INIT
sed -i "s/${GAME_EAC_LAUNCHER}/${CLIENT_GAME}/" "${WORK_DIR}/${GAME_INIT_FILE}"
echo "The file ${GAME_INIT_FILE} was modified"

# Create symbolic link 
ln -s "${EAC_PATH}" "${GAME_DATA_PATH}"
echo "A symbolic link of ${EAC_SO_FILE} was created"
echo "Update completed"
