This script helps to prepare the configuration of Fall Guys on GNU Linux

## Video Tutorial

[![Fall Guys en GNU Linux - Instalación en Heroic Games Launcher](https://i.ytimg.com/vi/ejJnaNqwz2g/maxresdefault.jpg)](https://www.youtube.com/watch?v=ejJnaNqwz2g)

[https://youtu.be/ejJnaNqwz2g](https://youtu.be/ejJnaNqwz2g)

## Tutorial
1. Copy the FallGuysUpdate.sh file to the root of the game.  
2. Provide execution permission. 
	```  
	chmod +x FallGuysUpdate.sh
	```
3. Run FallGuysUpdate.sh
	```
	./FallGuysUpdate.sh
	```

If you want to keep the script in a different path you must modify "WORK_DIR" entering the root path where the game is located
```
WORK_DIR="/game/path"
```
continue with step 2.
