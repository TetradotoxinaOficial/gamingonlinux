# GamingOnLinux
In this repository you will find scripts and tutorials to automate or maintain games on GNU Linux

## Social networks
*  [Web](https://tetradotoxina.com)
*  [Discord](https://discord.gg/6z3P4kY)
*  [Twitch](https://www.twitch.tv/tetradotoxina)
*  [Youtube Oficial](https://www.youtube.com/channel/UCKGueVQMO7_YhifvFuRBb6g)
*  [Youtube Live (Developers)](https://www.youtube.com/@tetradotoxinalive)
*  [Fanpage Oficial (Entertainment)](https://bit.ly/3wHqavn)
*  [Fanpage Developers (Developers)](https://bit.ly/3upIG9V)
*  [Facebook Group (Developers)](https://bit.ly/3bTZaAQ)
